package com.company;

import java.util.HashMap;
import java.util.Map;

public class Generator {

    public static Map<String, Integer> getMapDishes() {
        Map<String, Integer> mapDishes = new HashMap<>();
        mapDishes.put("Soup", 25);
        mapDishes.put("Potatous", 20);
        mapDishes.put("Ragu", 15);
        mapDishes.put("Tee", 10);
        mapDishes.put("Tiramisu", 30);
        mapDishes.put("Plate of cheese", 35);
        mapDishes.put("Water", 5);
        return mapDishes;
    }

    public static Map<String, Integer> getMapAmountDishes() {
        Map<String, Integer> mapDishes = new HashMap<>();
        mapDishes.put("Soup", 0);
        mapDishes.put("Potatous", 0);
        mapDishes.put("Ragu", 0);
        mapDishes.put("Tee", 0);
        mapDishes.put("Tiramisu", 0);
        mapDishes.put("Plate of cheese", 0);
        mapDishes.put("Water", 0);
        return mapDishes;
    }


}
