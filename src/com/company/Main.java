package com.company;

import java.util.Map;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Map<String, Integer> mapDishes = Generator.getMapDishes();
        Map<String, Integer> amountDishes = Generator.getMapAmountDishes();

        for (Map.Entry<String, Integer> entry : mapDishes.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }

        System.out.println("Выберете блюдо и колличество(название блюда/кол-во): ");
        Scanner valueOrder = new Scanner(System.in);
        while (!valueOrder.nextLine().equals("0")) {
            String value = valueOrder.nextLine();
            String[] dishesAmount = value.split("\\/");
            String nameDishes = dishesAmount[0];
            int amounttDishes = Integer.parseInt(dishesAmount[1]);
            System.out.println("Введенное значение: " + value);
            for (Map.Entry<String, Integer> entry : amountDishes.entrySet()) {
                if (entry.getKey().equalsIgnoreCase(nameDishes)) {
                    entry.setValue(entry.getValue() + amounttDishes);
                    break;
                }
            }
        }

        System.out.println("это весь заказ:");
        for (Map.Entry<String, Integer> entry : amountDishes.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }

        int finalAmount = 0;
        for (Map.Entry<String, Integer> entry : mapDishes.entrySet()) {
            for (Map.Entry<String, Integer> amountEntry : amountDishes.entrySet()) {
                if (entry.getKey().equalsIgnoreCase(amountEntry.getKey())) {
                    finalAmount += entry.getValue() * amountEntry.getValue();
                }
            }
        }
        System.out.println("final: " + finalAmount + " грн.");

    }
}
